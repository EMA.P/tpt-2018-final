var config = require('../../nightwatch.conf.js');

module.exports = {
    'Google': function(browser) {
        browser
            .url('https://google.com/')
            .waitForElementVisible('body div#main')
            .setValue('input[type=text]', 'tallinn')
            .pause(2000)
            .click('input[type=submit]')
            .waitForElementVisible('body div#main')
            .assert.containsText('.bkWMgd:first-child', 'Tallinn')
            .saveScreenshot(config.imgpath(browser) + 'searchResult.png')
            .click('.LC20lb')
            .waitForElementVisible('body div#main')
            .saveScreenshot(config.imgpath(browser) + 'google.png')
            .end();
    }
};
